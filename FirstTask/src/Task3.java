import java.util.Arrays;

public class Task3 {

        public static void main(String[] args) {
        int[] digits = {23, 56, 76, 45, 12, 56, 12, 45};
        average(digits);

        }

    public static int average(int[] input) {
        int averageValue;
        int sum = 0;
        int arrayLength = input.length;
        for (int i = 0; i < arrayLength; i++) {
            sum += input[i];

        }
        averageValue = sum / arrayLength;
        System.out.println(averageValue);
        return averageValue;
    }
}
